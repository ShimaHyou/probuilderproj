﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] Camera FPCamera;
    [SerializeField] float range;
    [SerializeField] float damage;

   bool canShoot = true;

    private void OnEnable()
    {
        canShoot = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T) && canShoot == true)
        {
            ProcessRayCast();
        }
    }

    private void ProcessRayCast()
    {
        RaycastHit hit;
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            Ennemies target = hit.transform.GetComponent<Ennemies>();
            if (target == null)
            {
                return;
            }
            target.EnemyHealth(damage);
        }
        else
        {
            return;
        }
    }
}
