﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemies : MonoBehaviour
{
    [SerializeField] float health;

    bool isDead = false;

    public bool IsDead()
    {
        return isDead;
    }

    public void EnemyHealth(float damage)
    {
        health -= damage;
        print("enemy health: " + health);
        if (health <= 0)
        {
            Die();
            Destroy(gameObject, 1.1f);
        }
    }
    void Die()
    {
        if (isDead)
        {
            return;
        }
        isDead = true;
    }
}
