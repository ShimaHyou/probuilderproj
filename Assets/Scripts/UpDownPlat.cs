﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownPlat : MonoBehaviour
{
    [SerializeField] Vector3 movementVector;
    [SerializeField] float period;

    [Range(0.20f, 1.30f)] [SerializeField] float movementFactor;

    Vector3 startingPos;

    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (period <= Mathf.Epsilon) 
        {
            return;
        }

        float cycle = Time.time / period; 

        const float tau = Mathf.PI * 2; //about 6.28
        float rawSinWave = Mathf.Sin(cycle * tau); //goes from -1 to +1

        movementFactor = rawSinWave / 2f + 0.7f;


        Vector3 offset = movementFactor * movementVector;
        transform.position = startingPos + offset;
    }
}
